//
//  CustomViewVM.swift
//  FRPDemoView
//
//  Created by Sumedh Ravi on 28/06/18.
//  Copyright © 2018 Sumedh Ravi. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class CustomViewVM {

    var centerCoordinates = BehaviorSubject<CGPoint?>(value: .zero)
    var backGroundColorObservable: Observable<UIColor>!
    
    init() {
        setupRx()
    }
    
    private func setupRx() {
        backGroundColorObservable = centerCoordinates.asObservable()
            .map({ (point) in
                guard let point = point else {
                    return UIColor.black
                }
                let red = ((point.x + point.y).truncatingRemainder(dividingBy: 255.0)) / 255.0
                let green = abs(point.x - point.y).truncatingRemainder(dividingBy: 255.0) / 255.0
                let blue = point.y.truncatingRemainder(dividingBy: point.x) / 255.0
                return UIColor(red: red, green: green, blue: blue, alpha: 1.0)
            })
    }
    
    func getComplementaryForColor(color: UIColor) -> UIColor {
        
        let ciColor = CIColor(color: color)
        // get the current values and make the difference from white
        let compRed: CGFloat = 1.0 - ciColor.red
        let compGreen: CGFloat = 1.0 - ciColor.green
        let compBlue: CGFloat = 1.0 - ciColor.blue
        
        return UIColor(red: compRed, green: compGreen, blue: compBlue, alpha: 1.0)
    }
}
