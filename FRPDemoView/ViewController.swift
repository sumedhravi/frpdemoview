//
//  ViewController.swift
//  FRPDemoView
//
//  Created by Sumedh Ravi on 28/06/18.
//  Copyright © 2018 Sumedh Ravi. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ViewController: UIViewController {

    @IBOutlet weak var centerButton: UIButton!
    private var customView: UIView?
    private var viewModel = CustomViewVM()
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createView()
        setupObservables()
    }
    
    private func createView() {
        customView?.removeFromSuperview()
        customView = UIView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 100, height: 100)))
        customView!.center = view.center
        customView!.layer.cornerRadius = 50.0
        view.addSubview(customView!)
        
        customView?.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleGestures)))
    }
    
    private func setupObservables() {
        customView?
            .rx.observe(CGPoint.self, "center")
            .bind(to: viewModel.centerCoordinates)
            .disposed(by: disposeBag)
        
        viewModel.backGroundColorObservable
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] (color) in
                UIView.animate(withDuration: 0.1, animations: {
                    self.customView?.backgroundColor = color
                    let backGroundColor = self.viewModel.getComplementaryForColor(color: color)
                    if backGroundColor != color {
                        self.view.backgroundColor = backGroundColor
                    }
                    self.centerButton.backgroundColor = color
                    self.centerButton.titleLabel?.textColor = backGroundColor
                })
            }).disposed(by: disposeBag)
        
        centerButton.rx.tap.observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] in 
                self.customView?.center = self.view.center
            })
            .disposed(by: disposeBag)
    }

    @objc private func handleGestures(_ sender: UIPanGestureRecognizer) {
        let location = sender.location(in: view)
        guard location.x - 50 > 0 && location.x + 50 < view.frame.width && location.y - 50 > 0 && location.y + 50 < view.frame.height else { return }
        UIView.animate(withDuration: 0.1) {
            self.customView?.center = location
        }
    }
}

